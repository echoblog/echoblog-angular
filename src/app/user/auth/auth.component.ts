import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { URLSearchParams } from 'url';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.less']
})
export class AuthComponent implements OnInit {
  selected = new FormControl(0);

  constructor(
    private r: Router,
    private activeRoute: ActivatedRoute
  ) {
    this.r.events.subscribe((e: any) => {
      switch (this.activeRoute.snapshot.queryParams.type) {
        case '0':
          this.selected.setValue(0);
          break;
        case '1':
          this.selected.setValue(1);
          break;
        default:
          break;
      }
    });

  }

  ngOnInit() {
  }

  titleAnimations(e: any) {
    const rXP = (e.pageX - e.target.offsetLeft - e.target.clientWidth / 2);
    const rYP = (e.pageY - e.target.offsetTop - e.target.clientHeight / 2);
    e.target.style.textShadow = rYP / 10 + 'px ' + rXP / 80 + 'px rgba(33,33,33,.8), ' + rYP / 8 + 'px ' + rXP / 60 + 'px #08c, ' + rXP / 70 + 'px ' + rYP / 12 + 'px rgba(0,159,227,.7)';
  }

}
