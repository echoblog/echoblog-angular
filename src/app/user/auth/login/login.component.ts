import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormControlName, ValidatorFn } from '@angular/forms';
import { UsersService, UserResponse } from 'src/app/services/users.service';
import { CommonResponse } from 'src/app/services/interfaces/commons';
import { MatSnackBar } from '@angular/material';
import { RouterUtils } from 'src/app/utils/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginLoginComponent implements OnInit {
  @Input() formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private usersSrv: UsersService,
    private snackBar: MatSnackBar,
    private rUtils: RouterUtils
  ) {
    this.formGroup = this.formBuilder.group({
      'email': new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9a-zA-Z!@%#]+$'),
      ])
    });
  }

  ngOnInit() {
    this.usersSrv.get().subscribe(
      (res: UserResponse) => {
        if (res.errcode === undefined && res.user.id !== undefined) {
          this.rUtils.ToIndex();
        }
      }
    );
  }

  login() {
    if (this.formGroup.valid) {
      this.usersSrv.login(this.formGroup.get('email').value, this.formGroup.get('password').value).subscribe(
        (res: CommonResponse) => {
          if (res.errcode !== 0) {
            this.formGroup.setErrors({ srvErr: res.err });
          } else {
            // todo: login dashboard
            this.snackBar.open('LOGIN SUCCESSED', '');
            setTimeout(() => {
              this.rUtils.ToIndex();
            }, 1500);
          }
        }
      );
    }
  }

}
