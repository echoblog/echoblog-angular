import { HttpErrorResponse } from '@angular/common/http';
import { CommonResponse } from '../interfaces/commons';
import { ErrorCodeToString } from './errors';

export function CommonErrorHandler(err: HttpErrorResponse): CommonResponse {
    const r: CommonResponse = {
        err: ErrorCodeToString(-1),
        errcode: -1,
    }
    return r
}