import { Component, OnInit } from '@angular/core';
import { UsersService, UserResponse, User } from './services/users.service';
import { CommonResponse } from './services/interfaces/commons';
import { Router, NavigationStart, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  isLogin: boolean;
  user: User = null;

  constructor(
    private usersSrv: UsersService,
    private router: Router,
  ) {
    this.router.events.subscribe((e: any) => {

      if (e instanceof NavigationStart) {
        this.usersSrv.get().subscribe(
          (res: UserResponse) => {
            if (res.errcode === undefined && res.user.id !== undefined) {
              this.isLogin = true;
              this.user = res.user;
            } else {
              this.isLogin = false;
            }
          }
        );
      }

    });
  }

  ngOnInit() { }

  logout() {
    this.usersSrv.logout().subscribe((res: CommonResponse) => {
      window.location.reload();
    });
  }

  switch() {
    // Set our navigation extras object
    // that passes on our global query params and fragment
    const navigationExtras: NavigationExtras = {
      queryParams: { 'type': 0 }
    };

    // Redirect the user
    this.router.navigate(['/user/auth'], navigationExtras);
  }
}
