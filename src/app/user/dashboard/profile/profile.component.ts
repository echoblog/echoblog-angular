import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, ValidatorFn } from '@angular/forms';
import { UsersService, UserResponse, User } from 'src/app/services/users.service';
import { CommonResponse } from 'src/app/services/interfaces/commons';
import { RouterUtils } from 'src/app/utils/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
  @Input() formGroup: FormGroup;
  @Input() passwordFormGroup: FormGroup;
  user: User;

  constructor(
    private formBuilder: FormBuilder,
    private usersSrv: UsersService,
    private r: RouterUtils,
  ) {
    this.formGroup = this.formBuilder.group({
      'nickname': new FormControl('', [
        Validators.required,
      ]),
      'avatar': new FormControl('', [
        Validators.required,
        Validators.pattern('')// url pattern
      ]),
    });

    this.passwordFormGroup = this.formBuilder.group({
      'oldpassword': new FormControl('', [
        Validators.required,
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(64),
        Validators.pattern('^[0-9a-zA-Z!@%#]+$'),
      ])
    });
  }

  ngOnInit() {
    this.getUser();
    const confirmPasswordControl = new FormControl('', [
      Validators.required,
      this.passwordComparation(this.passwordFormGroup, 'password')
    ]);
    this.passwordFormGroup.addControl('confirmPass', confirmPasswordControl);
  }

  getUser() {
    this.usersSrv.get().subscribe((res: UserResponse) => {
      if (res.errcode === undefined && res.user.id !== undefined) {
        this.user = res.user;
      } else {
        this.user = null;
      }
    });
  }

  passwordComparation(group: FormGroup, controlName: string): ValidatorFn {
    return (control: FormControl) => {
      const myValue = control.value;
      const compareValue = group.controls[controlName].value;
      return (myValue === compareValue) ? null : { valueDifferentFrom: controlName };
    };
  }

  modifyPassword() {
    if (this.passwordFormGroup.valid) {
      this.usersSrv.modify(
        '',
        '',
        this.passwordFormGroup.get('oldpassword').value,
        this.passwordFormGroup.get('confirmPass').value,
      ).subscribe(
        (res: CommonResponse) => {
          if (res.errcode !== 0) {
            this.formGroup.setErrors({ srvErr: res.err });
          } else {
            this.r.ToIndex();
          }
        }
      );
    }
  }

  modifyWithOutPass() {
    if (this.formGroup.valid) {
      this.usersSrv.modify(this.formGroup.get('avatar').value, this.formGroup.get('nickname').value, '', '').subscribe(
        (res: CommonResponse) => {
          if (res.errcode !== 0) {
            this.formGroup.setErrors({ srvErr: res.err });
          } else {
            this.getUser();
          }
        }
      );
    }
  }
}
