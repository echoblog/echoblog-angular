import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { UserComponent } from './user/user.component';
import { AuthComponent } from './user/auth/auth.component';
import { PortalComponent } from './portal/portal.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { ProfileComponent } from './user/dashboard/profile/profile.component';
import { HomeComponent } from './user/dashboard/home/home.component';
import { UserAuthGuard } from './guards/user-auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: PortalComponent },
  { path: 'blog', component: BlogComponent },
  {
    path: 'user', component: UserComponent, children: [
      {
        path: 'auth', component: AuthComponent
      },
      {
        path: 'dashboard', component: DashboardComponent,
        canActivate: [UserAuthGuard],
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'home' },
          { path: 'profile', component: ProfileComponent },
          { path: 'home', component: HomeComponent },
        ]
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
