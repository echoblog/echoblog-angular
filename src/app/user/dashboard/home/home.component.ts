import { Component, OnInit } from '@angular/core';
import { User, UsersService, UserResponse } from 'src/app/services/users.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  user: User = null;
  constructor(
    private usrSrv: UsersService,
  ) { }

  ngOnInit() {
    this.usrSrv.get().subscribe((res: UserResponse) => {
      this.user = res.user;
    });
  }

}
