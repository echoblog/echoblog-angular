import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { CommonResponse } from './interfaces/commons';
import { ErrorCodeToString } from './utils/errors';
import { CommonErrorHandler } from './utils/handlers';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  servicePrefix = '/v1/users/';
  defaultHeaders: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(private http: HttpClient) {
  }

  register(e: string, p: string, s: number): Observable<CommonResponse> {
    const request: RegisterRequest = {
      email: e,
      password: p,
      sex: s,
    };

    return this.http.post(this.servicePrefix, request, { headers: this.defaultHeaders, }).pipe(
      switchMap(
        (res: any) => {
          const response: CommonResponse = {
            err: ErrorCodeToString(res.errcode),
            errcode: res.errcode,
          };
          return of(response);
        }
      ),
      catchError(
        (err: HttpErrorResponse) => {
          return of(CommonErrorHandler(err));
        }
      ),
    );
  }

  login(e: string, p: string): Observable<CommonResponse> {
    const request: LoginRequest = {
      email: e,
      password: p,
    };

    return this.http.post(this.servicePrefix + 'session/', request, { headers: this.defaultHeaders }).pipe(
      switchMap(
        (res: any) => {
          const response: CommonResponse = {
            err: ErrorCodeToString(res.errcode),
            errcode: res.errcode,
          };
          return of(response);
        }
      ),
      catchError(
        (err: HttpErrorResponse) => {
          return of(CommonErrorHandler(err));
        }
      ),
    );
  }

  get(): Observable<UserResponse> {
    return this.http.get(this.servicePrefix + 'session/', { headers: this.defaultHeaders }).pipe(
      switchMap((res: any) => {
        const response: UserResponse = {
          user: {
            avatar: res.avatar,
            email: res.email,
            id: res.id,
            lastLoginTime: res.last_login_time,
            nickName: res.nick_name,
            sex: res.sex,
            signinTime: res.signin_time,
          },
          err: ErrorCodeToString(res.errcode),
          errcode: res.errcode,
        };
        return of(response);
      }),
      catchError(
        (err: HttpErrorResponse) => {
          const r: UserResponse = {
            user: null,
            err: ErrorCodeToString(-1),
            errcode: -1,
          };
          return of(r);
        }
      ),
    );

  }

  modify(a: string, n: string, o: string, p: string) {
    const request: ModifyRequest = {
      avatar: a,
      nick_name: n,
      old_pass: o,
      password: p,
    };

    return this.http.put(this.servicePrefix, request, { headers: this.defaultHeaders }).pipe(
      switchMap(
        (res: any) => {
          const response: CommonResponse = {
            err: ErrorCodeToString(res.errcode),
            errcode: res.errcode,
          };
          return of(response);
        }
      ),
      catchError(
        (err: HttpErrorResponse) => {
          return of(CommonErrorHandler(err));
        }
      ),
    );
  }

  logout() {
    return this.http.delete(this.servicePrefix + 'session/', { headers: this.defaultHeaders }).pipe(
      switchMap(
        (res: any) => {
          const response: CommonResponse = {
            err: ErrorCodeToString(res.errcode),
            errcode: res.errcode,
          };
          return of(response);
        }
      ),
      catchError(
        (err: HttpErrorResponse) => {
          return of(CommonErrorHandler(err));
        }
      ),
    );
  }
}

// structure
export interface User {
  avatar: string;
  email: string;
  id: number;
  lastLoginTime: number;
  nickName: string;
  sex: number;
  signinTime: number;
}

// reigster
interface RegisterRequest {
  email: string;
  password: string;
  sex: number;
}


// login
interface LoginRequest {
  email: string;
  password: string;
}

// get
export interface UserResponse {
  user: User;
  err: string;
  errcode: number;
}

// modify
interface ModifyRequest {
  avatar: string;
  nick_name: string;
  old_pass: string;
  password: string;
}

