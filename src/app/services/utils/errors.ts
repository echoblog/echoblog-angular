export function ErrorCodeToString(errCode: number): string {
    let errStr = '';

    switch (errCode) {
        case -1:
            errStr = 'The connection to the server failed';
        case 1:
            errStr = 'Invalid request!';
            break;
        case 2:
            errStr = 'SIGNUP Internal error';
            break;
        case 3:
            errStr = 'GETUSER internal error';
            break;
        case 4:
            errStr = 'UPDATEUSER internal error';
            break;
        case 9:
            errStr = 'User already exists!';
            break;
        case 10:
            errStr = 'User not exists!';
            break;
        case 11:
            errStr = 'Incorrect password!';
            break;
        case 12:
            errStr = 'Internal server error';
            break;
        case 13:
            errStr = 'Forbidden request';
            break;
        case 14:
            errStr = 'Login failed';
            break;
        default:
            errStr = 'Unknown error';
            break;
    }

    return errStr;
}
