import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService, UserResponse } from '../services/users.service';
import { RouterUtils } from '../utils/router';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class UserAuthGuard implements CanActivate {

  constructor(
    private usrSrv: UsersService,
    private rUtils: RouterUtils,
    private snackBar: MatSnackBar,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.usrSrv.get().toPromise().then((res: UserResponse) => {
      if (res.errcode === undefined && res.user.id !== undefined) {
        return true;
      }
      this.snackBar.open('You should login first!', '');
      this.rUtils.ToLogin();
      return false;
    });
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

}
