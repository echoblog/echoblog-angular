import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, ValidatorFn } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import { CommonResponse } from 'src/app/services/interfaces/commons';
import { RouterUtils } from 'src/app/utils/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  @Input() formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private usersSrv: UsersService,
    private rUtils: RouterUtils,
    private snackBar: MatSnackBar,
  ) {
    this.formGroup = this.formBuilder.group({
      'email': new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(64),
        Validators.pattern('^[0-9a-zA-Z!@%#]+$'),
      ]),
      'sex': new FormControl(1, [
        Validators.required,
        Validators.min(0),
        Validators.max(1),
      ])
    });
  }

  ngOnInit() {
    const confirmPasswordControl = new FormControl('', [
      Validators.required,
      this.passwordComparation(this.formGroup, 'password')
    ]);
    this.formGroup.addControl('confirmPass', confirmPasswordControl);
  }

  passwordComparation(group: FormGroup, controlName: string): ValidatorFn {
    return (control: FormControl) => {
      const myValue = control.value;
      const compareValue = group.controls[controlName].value;
      return (myValue === compareValue) ? null : { valueDifferentFrom: controlName };
    };
  }

  // bussiness logic
  register() {
    if (this.formGroup.valid) {
      this.usersSrv.register(
        this.formGroup.get('email').value,
        this.formGroup.get('password').value,
        this.formGroup.get('sex').value
      ).subscribe(
        (res: CommonResponse) => {
          if (res.errcode !== 0) {
            this.formGroup.setErrors({ srvErr: res.err });
          } else {
            // success
            this.snackBar.open('RIGISTER SUCCESSED', '');
            this.rUtils.ToLogin();
          }
        });
    }
  }

}
