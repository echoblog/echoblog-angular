export interface CommonResponse {
    err: string;
    errcode: number;
}
